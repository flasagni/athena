#include "InDetTrackingGeometry/BeamPipeBuilder.h"
#include "InDetTrackingGeometry/SiLayerBuilder.h"
#include "InDetTrackingGeometry/TRT_LayerBuilder.h"
#include "InDetTrackingGeometry/RobustTrackingGeometryBuilder.h"
#include "InDetTrackingGeometry/StagedTrackingGeometryBuilder.h"

using namespace InDet;

DECLARE_COMPONENT( BeamPipeBuilder )
DECLARE_COMPONENT( SiLayerBuilder )
DECLARE_COMPONENT( TRT_LayerBuilder )
DECLARE_COMPONENT( RobustTrackingGeometryBuilder )
DECLARE_COMPONENT( StagedTrackingGeometryBuilder )

